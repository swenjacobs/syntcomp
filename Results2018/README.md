# SYNTCOMP 2018 Results
                                               
* `TLSF`/`AIGER` determines the specification format
* `real`/`synt` determines the goal (realizability check or synthesis)
* `seq`/`par` determines the execution mode (sequential or parallel)
* `legacy` means the tools are versions from previous years that have not been updated