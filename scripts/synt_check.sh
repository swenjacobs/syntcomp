#!/bin/bash

# Syntactic checker for ASCII aiger files (.aag)
# Syntcomp standard, extended comment field

# Written by Jens Kreber

if [ "$1" == "--fix-names" ]; then
    fixnames=yes
    input="$2"
elif [ "$1" == "--fix-comment" ]; then
    fixcomment=yes
    input="$2"
else
    input=$1
fi


es="Error: $input:"
ws="Warning: $input:"
is="Info: $input:"

if [ ! -w $input ]; then
    echo "$es Cannot acces ${input}. Be sure file exists and is writable!"
    exit 1
fi

# append newline if needed
comm=$',w\nQ\n'
`ed -s "$input" <<< "$comm"`


# Check header
read -a header <<< `grep '^aag ' "$input"`
if [ -z "$header" ]; then
    header_aig=`grep '^aig ' "$input"`
    if [ -n "$header_aig" ]; then
	echo "$es aig header found!"
	exit 1
    fi
    echo "$es No header found!"
    exit 1
fi

if [ "${header[4]}" != "1" ]; then
    echo "$es ${header[4]} outputs defined, but only 1 allowed!"
    exit 1
fi

# calculate sum
m=$((${header[2]} + ${header[3]} + ${header[5]}))
if [ "$m" != "${header[1]}" ]; then
    echo "$es M is not the sum of I, L and A!"
    exit 1
fi

first_sline=`tail -n +2 "$input" | grep -n  '^[^[:digit:]]' | head -n 1 | cut -d ':' -f 1`
if [ -z $first_sline ]; then
    first_sline=`wc -l $input | cut -d ' ' -f 1`
    ((first_sline++))
else
    ((first_sline += 1)) # aag line abgeschnitten
fi
#echo "First symbol line: $first_sline"
rest=$(tail -n +$first_sline "$input")
line=$first_sline
inputsread=0
latchesread=0
outputsread=0

function check_label () {
    if [ -z ${current[1]} ]; then
	    if [ -n "$fixnames" ]; then
		`echo -e "${line}s/\$/ label_$current/\n,w\nQ" | ed -s $input`
		((fixednames += 1))				
	    else
		echo "$ws Symbol \"$current\" has no name!!"
	    fi
    fi

}

function insert_line () { # (startcount, character, header content)
    if [ ! "$1" == "$3" ]; then
	if [ -n "$fixnames" ]; then
	    command=""
	    for ((x=$1; x < $3; x++)); do
		bline=$((line - 1))
		ccommand=$(echo -e "${bline}a\n${2}${x} label__${2}${x}\n.\n")
		command="$command""$ccommand"$'\n'
		((line += 1))
		((insertednames++))
	    done
	    lcommand=$(echo -e ",w\nQ\n")
	    command="${command}${lcommand}"
	    res=`ed -s $input <<< "$command"`
	else
	    echo "$es Expected $3 $2, but only $(( $1 )) found!"
	    exit 1
	fi
    fi
}

# read inputs
while read -a current; do
    if [[ $current =~ ^i[[:digit:]]+$ ]]; then
	check_label
	((inputsread += 1))
    else
	break
    fi
    ((line++))
done <<< "$rest"
insert_line "$inputsread" "i" "${header[2]}"

# read latches
rest=$(tail -n +$line "$input")
while read -a current; do
    if [[ $current =~ ^l[[:digit:]]+$ ]]; then
	check_label
	((latchesread += 1))
    else
	break
    fi
    ((line++))
done <<< "$rest"
insert_line "$latchesread" "l" "${header[3]}"

# read outputs
rest=$(tail -n +$line "$input")
while read -a current; do
       if [[ $current =~ ^o[[:digit:]]+$ ]]; then
	check_label
	((outputsread += 1))
    else
	   break
    fi
    ((line++))
done <<< "$rest"
insert_line "$outputsread" "o" "${header[4]}"

if [ -n "$fixednames" ]; then
    echo "$is Fixed $fixednames missing names in symbol table!"
fi

if [ -n "$insertednames" ]; then
    echo "$is Fixed $insertednames missing lines in symbol table!"
fi

# Check comment section
rest=$(tail -n +$line "$input")
while read current; do
    if [ "$current" == "c" ]; then
	foundc=y
	((line++))
	break
    elif [ -z "$current" ]; then
       ((line++))
    else
	if [ -n "$fixcomment" ]; then
	    echo -e "$line i\nc\n.\nw\nQ\n" | ed -s "$input"
	    echo "Placed a \"c\" before comment section"
	    foundc=y
	    ((line++))
	    break
	else
	    echo "$es File has comments, but no \"c\" character to start the comment section was found!"
	    exit 1
	fi
    fi
done <<< "$rest"

if [ -z "$foundc" ]; then
    echo "$ws File has no comment section!!"
    exit 2
fi

rest=$(tail -n +$line "$input")
syntline=$(grep -n '#!SYNTCOMP' <<< "$rest" | head -n 1 | cut -d ':' -f 1)

if [ -z $syntline ]; then
    echo "$ws File has no SYNTCOMP tags!"
    exit 2
else
    syntcomm=$(tail -n +$((syntline + 1)) <<< "$rest")
    found=0
       while read current; do
	if [[ "$current" =~ ^STATUS ]]; then
	    if [[ ! "$current" =~ ^STATUS[[:space:]]*:[[:space:]]*(realizable|unrealizable|unknown)$ ]]; then
		echo "$ws incorrect STATUS tag!"
	    fi
	    ((found += 1))
	elif [[ "$current" =~ ^SOLVED_IN ]]; then
	    if [[ ! "$current" =~ ^SOLVED_IN[[:space:]]*:[[:space:]]*([0-9]+\.[0-9]+[[:space:]]*\[[[:alnum:][:space:]]+\])(,[[:space:]]*[0-9]+\.[0-9]+[[:space:]]*\[[[:alnum:][:space:]]+\])*$ ]]; then
		echo "$ws incorrect SOLVED_IN tag!"
	    fi
	    ((found += 1))
	elif [[ "$current" =~ ^SOLVED_BY ]]; then
	    if [[ ! "$current" =~ ^SOLVED_BY[[:space:]]*:[[:space:]]*([0-9]+/[0-9]+[[:space:]]*\[[[:alnum:][:space:]]+\])(,[[:space:]]*[0-9]+/[0-9]+[[:space:]]*\[[[:alnum:][:space:]]+\])*$ ]]; then
		echo "$ws incorrect SOLVED_BY tag!"
	    fi
	    ((found += 1))
	elif [[ "$current" =~ ^#\.$ ]]; then
	    noend=1
	    break
	else
	    echo "$ws Unknown SYNTCOMP tag \"$current\"!"
	fi
    done <<< "$syntcomm"
    if [ ! $noend ]; then
	echo "$ws SYNTCOMP tag area was not ended correctly!"
    fi
    if [ "$found" != "3" ]; then
	echo "$ws Missing some important SYNTCOMP tags!"
    fi
fi



