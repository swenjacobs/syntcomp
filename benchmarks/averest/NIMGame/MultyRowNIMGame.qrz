// ======================================================================
//			NIM Game
// ======================================================================
// A couple of matches is distributed in rows on a board "Board" such that
// row "i" contains 2*i+1 matches. In alternation, two players A and B then
// then take an arbitrary number of matches from one row. The player who
// will take the last match has won the game.
//  In module NimGame below, the variable turnA holds iff its player A's
// turn, and in this case the inputs numA and rowA encode player A's
// move: player A will take numA matches from rowA. In case that this
// should not be possible, player A is asked once more for a legal move,
// otherwise, the next turn is player B's move, which is handled in the
// same way by the inputs numB and rowB, respectively.
// ======================================================================

#ifndef rows
#define rows 5u
#endif

#define size (rows+rows)
#define EndOfGame (forall(nat[sizeOf(rows-1u)] i=0u .. rows-1u) Board[i]==0u)
#define XOR(x,y) ((nat) ((bv)x xor (bv)y))


module NimGame(nat[sizeOf(rows)] ?rowA,rowB,
	       nat[sizeOf(size)] ?numA,numB,
	       nat[sizeOf(size)] &Board[rows], &NimSum,
	       bool &turnA)
implements NimGameSpec(Board,turnA)
{
// ----------------------------------------------------------------------
// Initialization
// ----------------------------------------------------------------------
   turnA = true;
   sequence(nat[sizeOf(rows-1u)] i=0u .. rows-1u)
      Board[i] = 2u*i+1u;

// ----------------------------------------------------------------------
// now play the game
// ----------------------------------------------------------------------
   while(!EndOfGame) {
      if(turnA) {
      if( (numA<=Board[rowA]) & (numA>0u)) {
            next(Board[rowA]) = Board[rowA]-numA;
            next(turnA) = false;
	        }
	
     	}
	else if (!turnA){   if((numB<=Board[rowB]) & (numB>0u)) {
            next(Board[rowB]) = Board[rowB]-numB;
            next(turnA) = true;
            }
	    }
	
	pause;
	}
}



spec NimGameSpec(nat[sizeOf(size-1u)] Board[rows],bool turnA) {
   Assert_A_will_always_win:
        A (G (((turnA -> EndOfGame | (numA<=Board[rowA] & numA>0u))) & (EndOfGame -> ! turnA)));
}

