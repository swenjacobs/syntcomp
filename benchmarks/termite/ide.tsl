/* IDE hard drive controller specification and operating system interface
   specification for device driver synthesis. 
   This file describes a GR(1) game played by a device driver for an IDE hard
   disk against its environment consisting of the hard disk and operating
   system.  
   
   There are three sections:
     * Variable declarations 
     * Initial states, goal and fairness declarations
     * Transition relation

   The variables are divided into state, label, and uncontrollable label. In
   any state, player 1 (the driver) picks values for all the label variables.
   Player 2 (the environment) then picks values for the uncontrollable label
   variables. This uniquely determines the next state. 
*/ 

//These are M4 macros. The spec is preprocessed with M4 first.
define(CMD_READ_DMA_EXT,  0xc8)
define(CMD_WRITE_DMA_EXT, 0x25)
define(CMD_SET_FEATURE,   0xef)
define(FEAT_WC,           0x02)
define(FEAT_NWC,          0x82)
define(FEAT_XFR_MODE,     0x03)
define(XM_ULTRA_DMA,      0x08)

define(RCMD,              0x08)
define(REG_FEATURE0,      0x01)
define(REG_SECTORS,       0x02)
define(REG_LBA_LOW,       0x03)
define(REG_LBA_MID,       0x04)
define(REG_LBA_HIGH,      0x05)
define(REG_DEV,           0x06)
define(REG_ERRCMD,        0x07)

define(RCTL,              0x01)
define(REG_CTLSTAT,       0x02)
define(REG_BM_STATUS,     0x02)
define(REG_BM_PRD,        0x04)
define(RDMA,              0x02)
define(REG_BM_COMMAND,    0x00)

==State
//State variables

//Variables may be declared as abs (abstract) or conc (concrete). Concrete
//variables are represented fully in the synthesizer. Abstracted variables are
//not represented fully and are subject to predicate abstraction. For example,
//below, "pioDMA" will be assigned to a BDD variable, however, "regFeature0"
//and "regFeature1" will not, but "regFeature0==regFeature1" may be, if the
//synthesis algorithm determines that this will be useful in solving the game. 

//There are three types: bit vectors (uint) single bits (boolean) and
//enumerations ({items})

//Device state
transferMode        : abs  uint<3>;
pioDMA              : conc bool;
wce                 : conc bool;
irqAsserted         : conc bool;
stInternal          : conc {idle, command, reset_signal, reset_ready};
stDMACmd            : conc {wait_bm_ready, bm_read_prd, bm_ready, dma_read};
stCommand           : conc {command_start, dma_cmd, set_features_cmd};
setFeatState        : conc {setFeatIdle, setFeatWC, setFeatNWC, setFeatXFR0, setFeatXFR1};
regFeature0         : abs  uint<8>;
regFeature1         : abs  uint<8>;
regSectors0         : abs  uint<8>;
regSectors1         : abs  uint<8>;
regLBALow0          : abs  uint<8>;
regLBALow1          : abs  uint<8>;
regLBAMid0          : abs  uint<8>;
regLBAMid1          : abs  uint<8>;
regLBAHigh0         : abs  uint<8>;
regLBAHigh1         : abs  uint<8>;
regDev_LBExt        : abs  uint<5>;
regDev_LBA          : abs  uint<1>;
regError            : abs  uint<8>;
regCommand          : abs  uint<8>;
regControl_NIEn     : conc uint<1>;
regControl_SRST     : conc uint<1>;
regControl_HOB      : conc uint<1>;
regStatus_ERR       : conc uint<1>;
regStatus_obs       : conc uint<2>;
regStatus_DRQ       : conc uint<1>;
regStatus_bit4      : conc uint<1>;
regStatus_DF        : conc uint<1>;
regStatus_DRDY      : conc uint<1>;
regStatus_BSY       : conc uint<2>;
regBMCommand_Start  : abs  uint<1>;
regBMCommand_RW     : abs  uint<1>;
regBMStatus_ACTV    : conc uint<1>;
regBMStatus_ERR     : conc uint<1>;
regBMStatus_IRQ     : conc uint<1>;
regBMStatus_resv    : conc uint<2>;
regBMStatus_DRV0CAP : conc uint<1>;
regBMStatus_DRV1CAP : conc uint<1>;
regBMStatus_SMPLX   : conc uint<1>;
regBMPRD            : abs  uint<32>;
bufAddr             : abs  uint<32>;
bufSectors          : abs  uint<16>;
readPrd             : conc uint<1>;
prdValid            : conc bool;
srstTimerSignalled  : conc bool;

//Operating system state
osState             : conc {os_init, os_reset, os_write_cache, os_idle, os_read_pending, os_write_pending, os_read_ack_succ, os_read_ack_fail, os_write_ack_succ, os_write_ack_fail, os_error};
os_lba0             : abs uint<8>;
os_lba1             : abs uint<8>;
os_lba2             : abs uint<8>;
os_lba3             : abs uint<8>;
os_lba4             : abs uint<8>;
os_lba5             : abs uint<8>;
os_sect0            : abs uint<8>;
os_sect1            : abs uint<8>;
os_buf              : abs uint<32>;

==Label
//Label variables

tag                 : conc {write8, write32, fillPrd, reset, os_req, ack_read_succ, ack_read_fail, ack_write_succ, ack_write_fail, class_event};
bank                : abs  uint<8>;
addr                : abs  uint<8>;
write8_val          : abs  uint<8>;
fillPrdAddr         : abs  uint<32>;
fillPrdNSect        : abs  uint<16>;

featWCClass         : conc bool;
featNWCClass        : conc bool;
featXFRClass        : conc bool;
busMasterClass      : conc bool;
cmdErr              : conc bool;
dmaStartClass       : conc bool;

==UncontrollableLabel
//Uncontrollable label variables

osReqType           : conc {read, write};
reqLBA0             : abs  uint<8>;
reqLBA1             : abs  uint<8>;
reqLBA2             : abs  uint<8>;
reqLBA3             : abs  uint<8>;
reqLBA4             : abs  uint<8>;
reqLBA5             : abs  uint<8>;
reqSect0            : abs  uint<8>;
reqSect1            : abs  uint<8>;
reqBuf              : abs  uint<32>;
transSuccess        : conc bool;

==Init
//Possible initial states of the transition system

setFeatState == setFeatIdle && 
osState == os_init &&
stDMACmd == wait_bm_ready

==Goal
//Semicolon separated list of goal states. The Game is winning if the driver
//can force the system to be in all of these sets infinitly often

osState == os_idle;

==Fair 
//Semicolon separated list of fairness conditions. The expressions below
//describe unfair states. All runs of the system that make one of these
//conditions true forever are excluded.

//There is no fairness condition in this example.

==Transitions
//The transition relation is given by update functions for each state variable.
//The next state of each variable is a function of the current state variables,
//controllable labels, and uncontrollable labels. The syntax is very simple -
//it's just nested case statements.

//Device state updates:
regFeature0 := case {
    tag==write8 && bank==RCMD && addr==REG_FEATURE0 : write8_val;
    true                                            : regFeature0;
};

regFeature1 := case {
    tag==write8 && bank==RCMD && addr==REG_FEATURE0 : write8_val;
    true                                            : regFeature1;
};

regSectors0 := case {
    tag==write8 && bank==RCMD && addr==REG_SECTORS : write8_val;
    true                                           : regSectors0;
};
         
regSectors1 := case {
    tag==write8 && bank==RCMD && addr==REG_SECTORS : regSectors0;
    true                                           : regSectors1;
};

regLBALow0 := case {
    tag==write8 && bank==RCMD && addr==REG_LBA_LOW : write8_val;
    true                                           : regLBALow0;
};

regLBALow1 := case {
    tag==write8 && bank==RCMD && addr==REG_LBA_LOW : regLBALow0;
    true                                           : regLBALow1;
};

regLBAMid0 := case {
    tag==write8 && bank==RCMD && addr==REG_LBA_MID : write8_val;
    true                                           : regLBAMid0;
};

regLBAMid1 := case {
    tag==write8 && bank==RCMD && addr==REG_LBA_MID : regLBAMid0;
    true                                           : regLBAMid1;
};

regLBAHigh0 := case {
    tag==write8 && bank==RCMD && addr==REG_LBA_HIGH : write8_val;
    true                                            : regLBAHigh0;
};

regLBAHigh1 := case {
    tag==write8 && bank==RCMD && addr==REG_LBA_HIGH : regLBAHigh0;
    true                                            : regLBAHigh1;
};

regDev_LBExt := case {
    tag==write8 && bank==RCMD && addr==REG_DEV : write8_val[0:4];
    true                                       : regDev_LBExt;
};

regDev_LBA := case {
    tag==write8 && bank==RCMD && addr==REG_DEV : write8_val[6:6];
    true                                       : regDev_LBA;
};

regCommand := case {
    tag==write8 && bank==RCMD && addr==REG_ERRCMD : write8_val;
    true                                          : regCommand;
};

stCommand := case {
    tag==write8 && bank==RCMD && addr==REG_ERRCMD : case {
        write8_val == CMD_SET_FEATURE                                     : set_features_cmd;
        write8_val == CMD_READ_DMA_EXT || write8_val == CMD_WRITE_DMA_EXT : dma_read;
        true                                                              : stCommand;
    };
    true : stCommand;
};

regControl_NIEn := case {
    tag==write8 && bank==RCTL && addr==REG_CTLSTAT : write8_val[1:1];
    true                                           : regControl_NIEn; 
};

regControl_HOB := case {
    tag==write8 && bank==RCTL && addr==REG_CTLSTAT                                                                                            : write8_val[7:7];
    tag==write8 && bank==RCMD && (addr==REG_FEATURE0 || addr==REG_SECTORS || addr==REG_LBA_LOW || addr==REG_LBA_MID || addr==REG_LBA_HIGH)    : 0;
    true                                                                                                                                      : regControl_HOB;
};

regBMCommand_Start := case {
    tag==write8 && bank==RDMA && addr==REG_BM_COMMAND : write8_val[0:0];
    true : regBMCommand_Start;
};

regBMCommand_RW := case {
    tag==write8 && bank==RDMA && addr==REG_BM_COMMAND : write8_val[3:3];
    true : regBMCommand_RW;
};

bufAddr := case {
    tag==fillPrd : fillPrdAddr;
    true         : bufAddr;
};

bufSectors := case {
    tag==fillPrd : fillPrdNSect;
    true         : bufSectors;
};

regBMPRD := case {
    tag==write32 && bank==RDMA && addr==REG_BM_PRD : case {
        stDMACmd!=wait_bm_ready : regBMPRD; 
        true                    : fillPrdAddr;
    };
    true : regBMPRD;
};

prdValid := case {
    tag==write32 && bank==RDMA && addr==REG_BM_PRD : case {
        stDMACmd!=wait_bm_ready : prdValid; 
        true                    : 1;
    };
    true : prdValid;
};

pioDMA := case {
    tag==write8 && bank==RCTL && addr==REG_CTLSTAT : case {
        regControl_SRST==0 && write8_val[2:2]==1 || regControl_SRST==1 && write8_val[2:2]==0 : case {
                write8_val[2:2] == 1      : 0;
                stInternal == reset_ready : 0;
                true                      : pioDMA;
            };
        true : pioDMA;
    };
    tag==class_event && setFeatState==setFeatXFR0 && featXFRClass==1 : 1;
    tag==class_event && setFeatState==setFeatXFR1 && featXFRClass==1 : 0;
    true                                         : pioDMA;
};

wce := case {
    tag==write8 && bank==RCTL && addr==REG_CTLSTAT : case {
        regControl_SRST==0 && write8_val[2:2]==1 || regControl_SRST==1 && write8_val[2:2]==0 : case {
                write8_val[2:2] == 1      : 1;
                stInternal == reset_ready : 1;
                true                      : wce;
            };
        true : wce;
    };
    tag==class_event && setFeatState==setFeatWC  && featWCClass==1  : 1;
    tag==class_event && setFeatState==setFeatNWC && featNWCClass==1 : 0;
    true : wce;
};

stInternal := case {
    tag==write8 && bank==RCTL && addr==REG_CTLSTAT : case {
        regControl_SRST==0 && write8_val[2:2]==1 || regControl_SRST==1 && write8_val[2:2]==0 : case {
                write8_val[2:2] == 1      : reset_ready;
                stInternal == reset_ready : idle;
                true                      : stInternal;
            };
        true : stInternal;
    };
    tag==write8 && bank==RCMD && addr==REG_ERRCMD : command;
    true                                          : stInternal;
};

irqAsserted := case {
    tag==write8 && bank==RCTL && addr==REG_CTLSTAT && regControl_NIEn==0 && regBMStatus_IRQ!=0 : 1;
    regControl_NIEn==0                                                                         : 0;
    true                                                                                       : irqAsserted;
};

regBMStatus_ERR := case {
    tag==write8  && bank==RDMA && addr==REG_BM_STATUS && write8_val[1:1]==1                     : 0;
    tag==write8  && bank==REG_BM_COMMAND && (stDMACmd != wait_bm_ready || stDMACmd != dma_cmd)  : 1;
    tag==write32 && bank==RDMA && stDMACmd != wait_bm_ready                                     : 1;
    tag==write8  && bank==RCTL && addr==REG_CTLSTAT && regControl_SRST==1 && write8_val[2:2]==0 : 0;
    true                                                                                        : regBMStatus_ERR
};

regBMStatus_IRQ := case {
    tag==write8 && bank==RDMA && addr==REG_BM_STATUS && write8_val[2:2]==1 : 0;
    true                                                                   : regBMStatus_IRQ;
};

regBMStatus_DRV0CAP := case {
    tag==write8 && bank==RDMA && addr==REG_BM_STATUS : write8_val[5:5];
    true                                             : regBMStatus_DRV0CAP;
};

regBMStatus_DRV1CAP := case {
    tag==write8 && bank==RDMA && addr==REG_BM_STATUS : write8_val[6:6];
    true                                             : regBMStatus_DRV1CAP;
};

setFeatState := case {
    tag==class_event && setFeatState==setFeatIdle && regFeature0==FEAT_WC                                         : setFeatWC;
    tag==class_event && setFeatState==setFeatIdle && regFeature0==FEAT_NWC                                        : setFeatNWC;
    tag==class_event && setFeatState==setFeatIdle && regFeature0==FEAT_XFR_MODE && regSectors0[3:7]==XM_ULTRA_DMA : setFeatXFR0;
    tag==class_event && setFeatState==setFeatIdle && regFeature0==FEAT_XFR_MODE                                   : setFeatXFR1;
    tag==class_event && setFeatState==setFeatWC   && featWCClass==1                                               : setFeatIdle;
    tag==class_event && setFeatState==setFeatNWC  && featNWCClass==1                                              : setFeatIdle;
    tag==class_event && setFeatState==setFeatXFR0 && featXFRClass==1                                              : setFeatIdle;
    tag==class_event && setFeatState==setFeatXFR1 && featXFRClass==1                                              : setFeatIdle;
    true                                                                                      : setFeatState;
};

define(transferMode3, (setFeatState==setFeatXFR1 && featXFRClass==1))
define(nwc,           (setFeatState==setFeatNWC  && featNWCClass==1))

transferMode := case {
    setFeatState==setFeatXFR0 : regSectors0[0:2];
    setFeatState==setFeatXFR1 : 3;
    true                      : transferMode;
};

stDMACmd := case {
    tag==write8 && bank==RDMA && addr==REG_BM_COMMAND : case {
        regBMCommand_Start==0 && write8_val[0:0]==1       : bm_read_prd;
        true                                              : stDMACmd;
    };
    stDMACmd==bm_read_prd : case {
        (regBMCommand_RW == 1 && regCommand != CMD_READ_DMA_EXT) || 
        (regBMCommand_RW == 0 && regCommand != CMD_WRITE_DMA_EXT) ||
        (regSectors0 != bufSectors[0:7]) ||
        (regSectors1 != bufSectors[8:15]) ||
        regDev_LBA != 1                          : stDMACmd; 
        dmaStartClass==1                         : bm_ready;
    };
    stDMACmd==bm_ready && busMasterClass==1 : wait_bm_ready;
    true : stDMACmd;
};

regControl_SRST := 0;

//OS state updates

define(bm_event, (stDMACmd==bm_ready && busMasterClass==1))

define(transferCorrect, (
    os_lba0==regLBALow0    &&
    os_lba1==regLBALow1    &&
    os_lba2==regLBAMid0    &&
    os_lba3==regLBAMid1    &&
    os_lba4==regLBAHigh0   &&
    os_lba5==regLBAHigh1   &&
    os_sect0==regSectors0  &&
    os_sect1==regSectors1  &&
    os_buf==bufAddr        &&
    regBMCommand_RW==$1
    )
)

os_lba0 := case {
    tag==os_req : reqLBA0;
    true        : os_lba0;
};

os_lba1 := case {
    tag==os_req : reqLBA1;
    true        : os_lba1;
};

os_lba2 := case {
    tag==os_req : reqLBA2;
    true        : os_lba2;
};

os_lba3 := case {
    tag==os_req : reqLBA3;
    true        : os_lba3;
};

os_lba4 := case {
    tag==os_req : reqLBA4;
    true        : os_lba4;
};

os_lba5 := case {
    tag==os_req : reqLBA5;
    true        : os_lba5;
};

os_sect0 := case {
    tag==os_req : reqSect0;
    true        : os_sect0;
};

os_sect1 := case {
    tag==os_req : reqSect1;
    true        : os_sect1;
};

os_buf := case {
    tag==os_req : reqBuf;
    true        : os_buf;
};

osState := case {
    osState==os_init && tag==reset                                                   : os_reset;
    osState==os_reset && nwc && tag==class_event                                     : os_write_cache;
    osState==os_write_cache && transferMode3 && tag==class_event                     : os_idle;
    osState==os_idle && tag==os_req && osReqType==read                               : os_read_pending;
    osState==os_idle && tag==os_req && osReqType==write                              : os_write_pending;
    osState==os_read_pending && bm_event && !transferCorrect(0) && tag==class_event  : os_error;
    osState==os_read_pending && bm_event && transSuccess==1 && tag==class_event      : os_read_ack_succ;
    osState==os_read_pending && bm_event && transSuccess==0 && tag==class_event      : os_read_ack_fail;
    osState==os_write_pending && bm_event && !transferCorrect(1) && tag==class_event : os_error;
    osState==os_write_pending && bm_event && transSuccess==1 && tag==class_event     : os_write_ack_succ;
    osState==os_write_pending && bm_event && transSuccess==0 && tag==class_event     : os_write_ack_fail;
    osState==os_read_ack_succ && tag==ack_read_succ                                  : os_idle;
    osState==os_read_ack_fail && tag==ack_read_fail                                  : os_idle;
    osState==os_write_ack_succ && tag==ack_write_succ                                : os_idle;
    osState==os_write_ack_fail && tag==ack_write_fail                                : os_idle;
    true                                                                             : osState;
};

