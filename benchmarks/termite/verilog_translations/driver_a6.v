// IDE hard drive controller specification and operating system interface
// specification for device driver synthesis.
// This file describes a GR(1) game played by a device driver for an IDE hard
// disk against its environment consisting of the hard disk and operating
// system.

`define CMD_READ_DMA_EXT  200
`define CMD_WRITE_DMA_EXT 37
`define CMD_SET_FEATURE   239
`define FEAT_WC           2
`define FEAT_NWC          130
`define FEAT_XFR_MODE     3
`define XM_ULTRA_DMA      8

`define RCMD              8
`define REG_FEATURE0      1
`define REG_SECTORS       2
`define REG_LBA_LOW       3
`define REG_LBA_MID       4
`define REG_LBA_HIGH      5
`define REG_DEV           6
`define REG_ERRCMD        7

`define RCTL              1
`define REG_CTLSTAT       2
`define REG_BM_STATUS     2
`define REG_BM_PRD        4
`define RDMA              2
`define REG_BM_COMMAND    0

//typedef enum {read=0, write=1} i_osReqType_enum;
`define read              0
`define write             1

//typedef enum {write8, write32, fillPrd, reset, os_req, ack_read_succ, ack_read_fail, ack_write_succ, ack_write_fail, class_event} controllable_tag_enum;
`define write8            0
`define write32           1
`define fillPrd           2
`define reset             3
`define os_req            4
`define ack_read_succ     5
`define ack_read_fail     6
`define ack_write_succ    7
`define ack_write_fail    8
`define class_event       9

//typedef enum {idle=0, command=1, reset_signal=2, reset_ready=3} state_stInternal_enum;
`define idle              0
`define command           1
`define reset_signal      2
`define reset_ready       3

//typedef enum {wait_bm_ready=0, dma_read=1, bm_read_prd=2, bm_ready=3} state_stDMACmd_enum;
`define wait_bm_ready     0
`define dma_read          1
`define bm_read_prd       2
`define bm_ready          3

//typedef enum {command_start=0, dma_cmd=1, set_features_cmd=2} state_stCommand_enum;
`define command_start     0
`define dma_cmd           1
`define set_features_cmd  2


//typedef enum {setFeatIdle=0, setFeatWC=1, setFeatNWC=2, setFeatXFR0=3, setFeatXFR1=4} state_setFeatState_enum;
`define setFeatIdle       0
`define setFeatWC         1
`define setFeatNWC        2
`define setFeatXFR0       3
`define setFeatXFR1       4

//typedef enum {os_init=0, os_reset=1, os_write_cache=2, os_idle=3, os_read_pending=4, os_write_pending=5, os_read_ack_succ=6, os_read_ack_fail=7, os_write_ack_succ=8, os_write_ack_fail=9, os_error=10} state_osState_enum;
`define os_init           0
`define os_reset          1
`define os_write_cache    2
`define os_idle           3
`define os_read_pending   4
`define os_write_pending  5
`define os_read_ack_succ  6
`define os_read_ack_fail  7
`define os_write_ack_succ 8
`define os_write_ack_fail 9
`define os_error          10

module ide_hard_drive_controller_2(
        o_err,
        i_clk,
        i_osReqType_conc,
        i_reqLBA0_abs,
        i_reqLBA1_abs,
        i_reqLBA2_abs,
        i_reqLBA3_abs,
        i_reqLBA4_abs,
        i_reqLBA5_abs,
        i_reqSect0_abs,
        i_reqSect1_abs,
        i_reqBuf_abs,
        i_transSuccess_conc,
        controllable_tag_conc,
        controllable_bank_abs,
        controllable_addr_abs,
        controllable_write8_val_abs,
        controllable_fillPrdAddr_abs,
        controllable_fillPrdNSect_abs,
        controllable_featWCClass_conc,
        controllable_featNWCClass_conc,
        controllable_featXFRClass_conc,
        controllable_busMasterClass_conc,
        controllable_cmdErr_conc,
        controllable_dmaStartClass_conc );

input i_clk;
input i_osReqType_conc ;
input [7:0] i_reqLBA0_abs ;
input [7:0] i_reqLBA1_abs ;
input [7:0] i_reqLBA2_abs ;
input [7:0] i_reqLBA3_abs ;
input [7:0] i_reqLBA4_abs ;
input [7:0] i_reqLBA5_abs ;
input [7:0] i_reqSect0_abs ;
input [7:0] i_reqSect1_abs ;
input [31:0] i_reqBuf_abs ;
input i_transSuccess_conc ;
input [3:0] controllable_tag_conc ;
input [7:0] controllable_bank_abs ;
input [7:0] controllable_addr_abs ;
input [7:0] controllable_write8_val_abs ;
input [31:0] controllable_fillPrdAddr_abs ;
input [15:0] controllable_fillPrdNSect_abs ;
input controllable_featWCClass_conc ;
input controllable_featNWCClass_conc ;
input controllable_featXFRClass_conc ;
input controllable_busMasterClass_conc ;
input controllable_cmdErr_conc ;
input controllable_dmaStartClass_conc ;
output o_err;

reg [2:0] state_transferMode_abs;
reg state_pioDMA_conc ;
reg state_wce_conc ;
reg state_irqAsserted_conc ;
reg [1:0] state_stInternal_conc ;
reg [1:0] state_stDMACmd_conc ;
reg [1:0] state_stCommand_conc ;
reg [2:0] state_setFeatState_conc ;
reg [7:0] state_regFeature0_abs ;
reg [7:0] state_regFeature1_abs ;
reg [7:0] state_regSectors0_abs ;
reg [7:0] state_regSectors1_abs ;
reg [7:0] state_regLBALow0_abs ;
reg [7:0] state_regLBALow1_abs ;
reg [7:0] state_regLBAMid0_abs ;
reg [7:0] state_regLBAMid1_abs ;
reg [7:0] state_regLBAHigh0_abs ;
reg [7:0] state_regLBAHigh1_abs ;
reg [4:0] state_regDev_LBExt_abs ;
reg state_regDev_LBA_abs ;
reg [7:0] state_regError_abs ;
reg [7:0] state_regCommand_abs ;
reg state_regControl_NIEn_conc ;
reg state_regControl_SRST_conc ;
reg state_regControl_HOB_conc ;
reg state_regStatus_ERR_conc ;
reg [1:0] state_regStatus_obs_conc ;
reg state_regStatus_DRQ_conc ;
reg state_regStatus_bit4_conc ;
reg state_regStatus_DF_conc ;
reg state_regStatus_DRDY_conc ;
reg [1:0] state_regStatus_BSY_conc ;
reg state_regBMCommand_Start_abs ;
reg state_regBMCommand_RW_abs ;
reg state_regBMStatus_ACTV_conc ;
reg state_regBMStatus_ERR_conc ;
reg state_regBMStatus_IRQ_conc ;
reg [1:0] state_regBMStatus_resv_conc ;
reg state_regBMStatus_DRV0CAP_conc ;
reg state_regBMStatus_DRV1CAP_conc ;
reg state_regBMStatus_SMPLX_conc ;
reg [31:0] state_regBMPRD_abs ;
reg [31:0] state_bufAddr_abs ;
reg [15:0] state_bufSectors_abs ;
reg state_readPrd_conc ;
reg state_prdValid_conc ;
reg state_srstTimerSignalled_conc ;

reg [3:0] state_osState_conc ;
reg [7:0] state_os_lba0_abs ;
reg [7:0] state_os_lba1_abs ;
reg [7:0] state_os_lba2_abs ;
reg [7:0] state_os_lba3_abs ;
reg [7:0] state_os_lba4_abs ;
reg [7:0] state_os_lba5_abs ;
reg [7:0] state_os_sect0_abs ;
reg [7:0] state_os_sect1_abs ;
reg [31:0] state_os_buf_abs ;

reg [2:0] fair_cnt;

wire transferMode3;
wire nwc;
wire bm_event;
wire transferCorrect;
wire transferCorrect_0;
wire transferCorrect_1;
wire buechi_satisfied;
wire [2:0] next_state_transferMode_abs;
wire next_state_pioDMA_conc ;
wire next_state_wce_conc ;
wire next_state_irqAsserted_conc ;
wire [1:0] next_state_stInternal_conc ;
wire [1:0] next_state_stDMACmd_conc ;
wire [1:0] next_state_stCommand_conc ;
wire [2:0] next_state_setFeatState_conc ;
wire [7:0] next_state_regFeature0_abs ;
wire [7:0] next_state_regFeature1_abs ;
wire [7:0] next_state_regSectors0_abs ;
wire [7:0] next_state_regSectors1_abs ;
wire [7:0] next_state_regLBALow0_abs ;
wire [7:0] next_state_regLBALow1_abs ;
wire [7:0] next_state_regLBAMid0_abs ;
wire [7:0] next_state_regLBAMid1_abs ;
wire [7:0] next_state_regLBAHigh0_abs ;
wire [7:0] next_state_regLBAHigh1_abs ;
wire [4:0] next_state_regDev_LBExt_abs ;
wire next_state_regDev_LBA_abs ;
wire [7:0] next_state_regError_abs ;
wire [7:0] next_state_regCommand_abs ;
wire next_state_regControl_NIEn_conc ;
wire next_state_regControl_SRST_conc ;
wire next_state_regControl_HOB_conc ;
wire next_state_regStatus_ERR_conc ;
wire [1:0] next_state_regStatus_obs_conc ;
wire next_state_regStatus_DRQ_conc ;
wire next_state_regStatus_bit4_conc ;
wire next_state_regStatus_DF_conc ;
wire next_state_regStatus_DRDY_conc ;
wire [1:0] next_state_regStatus_BSY_conc ;
wire next_state_regBMCommand_Start_abs ;
wire next_state_regBMCommand_RW_abs ;
wire next_state_regBMStatus_ACTV_conc ;
wire next_state_regBMStatus_ERR_conc ;
wire next_state_regBMStatus_IRQ_conc ;
wire [1:0] next_state_regBMStatus_resv_conc ;
wire next_state_regBMStatus_DRV0CAP_conc ;
wire next_state_regBMStatus_DRV1CAP_conc ;
wire next_state_regBMStatus_SMPLX_conc ;
wire [31:0] next_state_regBMPRD_abs ;
wire [31:0] next_state_bufAddr_abs ;
wire [15:0] next_state_bufSectors_abs ;
wire next_state_readPrd_conc ;
wire next_state_prdValid_conc ;
wire next_state_srstTimerSignalled_conc ;

wire [3:0] next_state_osState_conc ;
wire [7:0] next_state_os_lba0_abs ;
wire [7:0] next_state_os_lba1_abs ;
wire [7:0] next_state_os_lba2_abs ;
wire [7:0] next_state_os_lba3_abs ;
wire [7:0] next_state_os_lba4_abs ;
wire [7:0] next_state_os_lba5_abs ;
wire [7:0] next_state_os_sect0_abs ;
wire [7:0] next_state_os_sect1_abs ;
wire [31:0] next_state_os_buf_abs ;

// some abbreviations:
assign transferMode3 = (state_setFeatState_conc==`setFeatXFR1 && controllable_featXFRClass_conc==1);
assign nwc =           (state_setFeatState_conc==`setFeatNWC  && controllable_featNWCClass_conc==1);
assign bm_event = (state_stDMACmd_conc==`bm_ready && controllable_busMasterClass_conc==1);
assign transferCorrect = (state_os_lba0_abs==state_regLBALow0_abs    &&
                          state_os_lba1_abs==state_regLBALow1_abs    &&
                          state_os_lba2_abs==state_regLBAMid0_abs    &&
                          state_os_lba3_abs==state_regLBAMid1_abs    &&
                          state_os_lba4_abs==state_regLBAHigh0_abs   &&
                          state_os_lba5_abs==state_regLBAHigh1_abs   &&
                          state_os_sect0_abs==state_regSectors0_abs  &&
                          state_os_sect1_abs==state_regSectors1_abs  &&
                          state_os_buf_abs==state_bufAddr_abs);
assign transferCorrect_0 = (transferCorrect      &&
                            state_regBMCommand_RW_abs==0);
assign transferCorrect_1 = (transferCorrect      &&
                            state_regBMCommand_RW_abs==1);


// state updates:
  //Device state updates:
assign next_state_regFeature0_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_FEATURE0) ? controllable_write8_val_abs  : state_regFeature0_abs;
assign next_state_regFeature1_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_FEATURE0) ? controllable_write8_val_abs  : state_regFeature1_abs;
assign next_state_regSectors0_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_SECTORS)  ? controllable_write8_val_abs  : state_regSectors0_abs;
assign next_state_regSectors1_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_SECTORS)  ? state_regSectors0_abs : state_regSectors1_abs;
assign next_state_regLBALow0_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_LOW)  ? controllable_write8_val_abs  : state_regLBALow0_abs;
assign next_state_regLBALow1_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_LOW)  ? state_regLBALow0_abs  : state_regLBALow1_abs;
assign next_state_regLBAMid0_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_MID)  ? controllable_write8_val_abs  : state_regLBAMid0_abs;
assign next_state_regLBAMid1_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_MID)  ? state_regLBAMid0_abs  : state_regLBAMid1_abs;
assign next_state_regLBAHigh0_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_HIGH) ? controllable_write8_val_abs  : state_regLBAHigh0_abs;
assign next_state_regLBAHigh1_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_HIGH) ? state_regLBAHigh0_abs : state_regLBAHigh1_abs;
assign next_state_regDev_LBExt_abs = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_DEV)      ? controllable_write8_val_abs[4:0] : state_regDev_LBExt_abs;
assign next_state_regDev_LBA_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_DEV)      ? controllable_write8_val_abs[6:6] : state_regDev_LBA_abs;
assign next_state_regCommand_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_ERRCMD)   ? controllable_write8_val_abs  : state_regCommand_abs;
assign next_state_stCommand_conc    = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_ERRCMD)   ? ((controllable_write8_val_abs == `CMD_SET_FEATURE) ? `set_features_cmd : ((controllable_write8_val_abs == `CMD_READ_DMA_EXT || controllable_write8_val_abs == `CMD_WRITE_DMA_EXT) ? `dma_cmd : state_stCommand_conc)) : state_stCommand_conc;
assign next_state_regControl_NIEn_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT) ? controllable_write8_val_abs[1:1] : state_regControl_NIEn_conc;
assign next_state_regControl_HOB_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT) ? controllable_write8_val_abs[7:7] : ((controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && (controllable_addr_abs==`REG_FEATURE0 || controllable_addr_abs==`REG_SECTORS || controllable_addr_abs==`REG_LBA_LOW || controllable_addr_abs==`REG_LBA_MID || controllable_addr_abs==`REG_LBA_HIGH)) ? 0 : state_regControl_HOB_conc);
assign next_state_regBMCommand_Start_abs = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_COMMAND) ? controllable_write8_val_abs[0:0] : state_regBMCommand_Start_abs;
assign next_state_regBMCommand_RW_abs = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_COMMAND) ? controllable_write8_val_abs[3:3] : state_regBMCommand_RW_abs;
assign next_state_bufAddr_abs = (controllable_tag_conc==`fillPrd) ? controllable_fillPrdAddr_abs : state_bufAddr_abs;
assign next_state_bufSectors_abs = (controllable_tag_conc==`fillPrd) ? controllable_fillPrdNSect_abs : state_bufSectors_abs;
assign next_state_regBMPRD_abs = (controllable_tag_conc==`write32 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_PRD) ? ((state_stDMACmd_conc!=`wait_bm_ready) ? state_regBMPRD_abs : controllable_fillPrdAddr_abs) : state_regBMPRD_abs;
assign next_state_prdValid_conc = (controllable_tag_conc==`write32 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_PRD) ? ((state_stDMACmd_conc!=`wait_bm_ready) ? state_prdValid_conc : 1) : state_prdValid_conc;
assign next_state_pioDMA_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT) ?
             (
               (state_regControl_SRST_conc==0 && controllable_write8_val_abs[2:2]==1 || state_regControl_SRST_conc==1 && controllable_write8_val_abs[2:2]==0) ?
               ((controllable_write8_val_abs[2:2] == 1 || state_stInternal_conc == `reset_ready) ? 0 : state_pioDMA_conc) :
               state_pioDMA_conc
             ) :
             (
              (controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatXFR0 && controllable_featXFRClass_conc==1) ? 1 :
               ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatXFR1 && controllable_featXFRClass_conc==1) ? 0 : state_pioDMA_conc)
             );
assign next_state_wce_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT) ?
             (
               (state_regControl_SRST_conc==0 && controllable_write8_val_abs[2:2]==1 || state_regControl_SRST_conc==1 && controllable_write8_val_abs[2:2]==0) ?
               ((controllable_write8_val_abs[2:2] == 1 || state_stInternal_conc == `reset_ready) ? 1 : state_wce_conc) :
               state_wce_conc
             ) :
             (
              (controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatWC  && controllable_featWCClass_conc==1) ? 1 :
               ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatNWC && controllable_featNWCClass_conc==1) ? 0 : state_wce_conc)
             );

assign next_state_stInternal_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT) ?
             (
               (state_regControl_SRST_conc==0 && controllable_write8_val_abs[2:2]==1 || state_regControl_SRST_conc==1 && controllable_write8_val_abs[2:2]==0) ?
               ((controllable_write8_val_abs[2:2] == 1) ? `reset_ready : ((state_stInternal_conc == `reset_ready) ? `idle : state_stInternal_conc)) :
               state_stInternal_conc
             ) :
             (
              (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_ERRCMD) ? `command : state_stInternal_conc
             );

assign next_state_irqAsserted_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT && state_regControl_NIEn_conc==0 && state_regBMStatus_IRQ_conc!=0) ? 1 : ((state_regControl_NIEn_conc==0) ? 0 : state_irqAsserted_conc);

assign next_state_regBMStatus_ERR_conc = (controllable_tag_conc==`write8  && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_STATUS && controllable_write8_val_abs[1:1]==1) ? 0 :
                    ((controllable_tag_conc==`write8  && controllable_bank_abs==`REG_BM_COMMAND && (state_stDMACmd_conc != `wait_bm_ready || state_stDMACmd_conc != `dma_read)) ? 1 :
                    ((controllable_tag_conc==`write32 && controllable_bank_abs==`RDMA && state_stDMACmd_conc != `wait_bm_ready) ? 1 :
                    ((controllable_tag_conc==`write8  && controllable_bank_abs==`RCTL && controllable_addr_abs==`REG_CTLSTAT && state_regControl_SRST_conc==1 && controllable_write8_val_abs[2:2]==0) ? 0 : state_regBMStatus_ERR_conc)));

assign next_state_regBMStatus_IRQ_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_STATUS && controllable_write8_val_abs[2:2]==1) ? 0 : state_regBMStatus_IRQ_conc;
assign next_state_regBMStatus_DRV0CAP_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_STATUS) ? controllable_write8_val_abs[5:5] : state_regBMStatus_DRV0CAP_conc;
assign next_state_regBMStatus_DRV1CAP_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_STATUS) ? controllable_write8_val_abs[6:6] : state_regBMStatus_DRV1CAP_conc;



assign next_state_setFeatState_conc = (controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_WC) ? `setFeatWC :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_NWC) ? `setFeatNWC :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_XFR_MODE && state_regSectors0_abs[7:3]==`XM_ULTRA_DMA) ? `setFeatXFR0 :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_XFR_MODE) ? `setFeatXFR1 :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatWC   && controllable_featWCClass_conc==1) ? `setFeatIdle :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatNWC  && controllable_featNWCClass_conc==1) ? `setFeatIdle :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatXFR0 && controllable_featXFRClass_conc==1) ? `setFeatIdle :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatXFR1 && controllable_featXFRClass_conc==1) ? `setFeatIdle : state_setFeatState_conc)))))));

assign next_state_transferMode_abs = (state_setFeatState_conc==`setFeatXFR0) ? state_regSectors0_abs[2:0] :
                 ((state_setFeatState_conc==`setFeatXFR1) ? 3 : state_transferMode_abs);

assign next_state_stDMACmd_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_COMMAND) ?
               (
                 (state_regBMCommand_Start_abs==0 && controllable_write8_val_abs[0:0]==1) ? `bm_read_prd : state_stDMACmd_conc
               ) :
               (
                 (state_stDMACmd_conc==`bm_read_prd) ?
                 (((state_regBMCommand_RW_abs == 1 && state_regCommand_abs != `CMD_READ_DMA_EXT) || (state_regBMCommand_RW_abs == 0 && state_regCommand_abs != `CMD_WRITE_DMA_EXT) || (state_regSectors0_abs != state_bufSectors_abs[7:0]) || (state_regSectors1_abs != state_bufSectors_abs[15:8]) || state_regDev_LBA_abs != 1) ? state_stDMACmd_conc : ((controllable_dmaStartClass_conc==1) ? `bm_ready : state_stDMACmd_conc)) :
                 ((state_stDMACmd_conc==`bm_ready && controllable_busMasterClass_conc==1) ? `wait_bm_ready : state_stDMACmd_conc)
               );

assign next_state_regControl_SRST_conc = 0;

//OS state updates
assign next_state_os_lba0_abs = (controllable_tag_conc==`os_req) ? i_reqLBA0_abs : state_os_lba0_abs;
assign next_state_os_lba1_abs = (controllable_tag_conc==`os_req) ? i_reqLBA1_abs : state_os_lba1_abs;
assign next_state_os_lba2_abs = (controllable_tag_conc==`os_req) ? i_reqLBA2_abs : state_os_lba2_abs;
assign next_state_os_lba3_abs = (controllable_tag_conc==`os_req) ? i_reqLBA3_abs : state_os_lba3_abs;
assign next_state_os_lba4_abs = (controllable_tag_conc==`os_req) ? i_reqLBA4_abs : state_os_lba4_abs;
assign next_state_os_lba5_abs = (controllable_tag_conc==`os_req) ? i_reqLBA5_abs : state_os_lba5_abs;
assign next_state_os_sect0_abs = (controllable_tag_conc==`os_req) ? i_reqSect0_abs : state_os_sect0_abs;
assign next_state_os_sect1_abs = (controllable_tag_conc==`os_req) ? i_reqSect1_abs : state_os_sect1_abs;
assign next_state_os_buf_abs = (controllable_tag_conc==`os_req) ? i_reqBuf_abs : state_os_buf_abs;

assign next_state_osState_conc = (state_osState_conc==`os_init && controllable_tag_conc==`reset) ? `os_reset :
      ((state_osState_conc==`os_reset && nwc && controllable_tag_conc==`class_event) ? `os_write_cache :
      ((state_osState_conc==`os_write_cache && transferMode3 && controllable_tag_conc==`class_event) ? `os_idle :
      ((state_osState_conc==`os_idle && controllable_tag_conc==`os_req && i_osReqType_conc==`read) ? `os_read_pending :
      ((state_osState_conc==`os_idle && controllable_tag_conc==`os_req && i_osReqType_conc==`write) ? `os_write_pending :
      ((state_osState_conc==`os_read_pending && bm_event && !transferCorrect_0 && controllable_tag_conc==`class_event) ? `os_error :
      ((state_osState_conc==`os_read_pending && bm_event && i_transSuccess_conc==1 && controllable_tag_conc==`class_event) ? `os_read_ack_succ :
      ((state_osState_conc==`os_read_pending && bm_event && i_transSuccess_conc==0 && controllable_tag_conc==`class_event) ? `os_read_ack_fail :
      ((state_osState_conc==`os_write_pending && bm_event && !transferCorrect_1 && controllable_tag_conc==`class_event) ? `os_error :
      ((state_osState_conc==`os_write_pending && bm_event && i_transSuccess_conc==1 && controllable_tag_conc==`class_event) ? `os_write_ack_succ :
      ((state_osState_conc==`os_write_pending && bm_event && i_transSuccess_conc==0 && controllable_tag_conc==`class_event) ? `os_write_ack_fail :
      ((state_osState_conc==`os_read_ack_succ && controllable_tag_conc==`ack_read_succ) ? `os_idle :
      ((state_osState_conc==`os_read_ack_fail && controllable_tag_conc==`ack_read_fail) ? `os_idle :
      ((state_osState_conc==`os_write_ack_succ && controllable_tag_conc==`ack_write_succ) ? `os_idle :
      ((state_osState_conc==`os_write_ack_fail && controllable_tag_conc==`ack_write_fail) ? `os_idle : state_osState_conc))))))))))))));


// buechi-to-safety construction:                            
assign buechi_satisfied = (state_osState_conc == `os_idle);                          
                            
assign o_err = (fair_cnt >= 6) || controllable_tag_conc >= 10;

initial
 begin
  state_transferMode_abs = 0;
  state_pioDMA_conc = 0;
  state_wce_conc = 0;
  state_irqAsserted_conc = 0;
  state_stInternal_conc = `idle;
  state_stDMACmd_conc = `wait_bm_ready;
  state_stCommand_conc = `command_start;
  state_setFeatState_conc = `setFeatIdle;
  state_regFeature0_abs = 0;
  state_regFeature1_abs = 0;
  state_regSectors0_abs = 0;
  state_regSectors1_abs = 0;
  state_regLBALow0_abs = 0;
  state_regLBALow1_abs = 0;
  state_regLBAMid0_abs = 0;
  state_regLBAMid1_abs = 0;
  state_regLBAHigh0_abs = 0;
  state_regLBAHigh1_abs = 0;
  state_regDev_LBExt_abs = 0;
  state_regDev_LBA_abs = 0;
  state_regError_abs = 0;
  state_regCommand_abs = 0;
  state_regControl_NIEn_conc = 0;
  state_regControl_SRST_conc = 0;
  state_regControl_HOB_conc = 0;
  state_regStatus_ERR_conc = 0;
  state_regStatus_obs_conc = 0;
  state_regStatus_DRQ_conc = 0;
  state_regStatus_bit4_conc = 0;
  state_regStatus_DF_conc = 0;
  state_regStatus_DRDY_conc = 0;
  state_regStatus_BSY_conc = 0;
  state_regBMCommand_Start_abs = 0;
  state_regBMCommand_RW_abs = 0;
  state_regBMStatus_ACTV_conc = 0;
  state_regBMStatus_ERR_conc = 0;
  state_regBMStatus_IRQ_conc = 0;
  state_regBMStatus_resv_conc = 0;
  state_regBMStatus_DRV0CAP_conc = 0;
  state_regBMStatus_DRV1CAP_conc = 0;
  state_regBMStatus_SMPLX_conc = 0;
  state_regBMPRD_abs = 0;
  state_bufAddr_abs = 0;
  state_bufSectors_abs = 0;
  state_readPrd_conc = 0;
  state_prdValid_conc = 0;
  state_srstTimerSignalled_conc = 0;
  state_osState_conc = `os_init;
  state_os_lba0_abs = 0;
  state_os_lba1_abs = 0;
  state_os_lba2_abs = 0;
  state_os_lba3_abs = 0;
  state_os_lba4_abs = 0;
  state_os_lba5_abs = 0;
  state_os_sect0_abs = 0;
  state_os_sect1_abs = 0;
  state_os_buf_abs = 0;
  fair_cnt = 0;
 end

always @(posedge i_clk)
 begin

  if(buechi_satisfied)
   begin
    fair_cnt = 0;
   end
  else
   begin
    fair_cnt = fair_cnt + 1;
   end
 
  //Device state updates:
  state_regFeature0_abs          = next_state_regFeature0_abs ;
  state_regFeature1_abs          = next_state_regFeature1_abs ;
  state_regSectors0_abs          = next_state_regSectors0_abs ;
  state_regSectors1_abs          = next_state_regSectors1_abs ;
  state_regLBALow0_abs           = next_state_regLBALow0_abs ;
  state_regLBALow1_abs           = next_state_regLBALow1_abs ;
  state_regLBAMid0_abs           = next_state_regLBAMid0_abs ;
  state_regLBAMid1_abs           = next_state_regLBAMid1_abs ;
  state_regLBAHigh0_abs          = next_state_regLBAHigh0_abs ;
  state_regLBAHigh1_abs          = next_state_regLBAHigh1_abs ;
  state_regDev_LBExt_abs         = next_state_regDev_LBExt_abs ;
  state_regDev_LBA_abs           = next_state_regDev_LBA_abs ;
  state_regCommand_abs           = next_state_regCommand_abs ;
  state_stCommand_conc           = next_state_stCommand_conc ;
  state_regControl_NIEn_conc     = next_state_regControl_NIEn_conc ;
  state_regControl_HOB_conc      = next_state_regControl_HOB_conc ;
  state_regBMCommand_Start_abs   = next_state_regBMCommand_Start_abs ;
  state_regBMCommand_RW_abs      = next_state_regBMCommand_RW_abs ;
  state_bufAddr_abs              = next_state_bufAddr_abs ;
  state_bufSectors_abs           = next_state_bufSectors_abs ;
  state_regBMPRD_abs             = next_state_regBMPRD_abs ;
  state_prdValid_conc            = next_state_prdValid_conc ;
  state_pioDMA_conc              = next_state_pioDMA_conc ;
  state_wce_conc                 = next_state_wce_conc ;
  state_stInternal_conc          = next_state_stInternal_conc ;
  state_irqAsserted_conc         = next_state_irqAsserted_conc ;
  state_regBMStatus_ERR_conc     = next_state_regBMStatus_ERR_conc ;
  state_regBMStatus_IRQ_conc     = next_state_regBMStatus_IRQ_conc ;
  state_regBMStatus_DRV0CAP_conc = next_state_regBMStatus_DRV0CAP_conc ;
  state_regBMStatus_DRV1CAP_conc = next_state_regBMStatus_DRV1CAP_conc ;
  state_setFeatState_conc        = next_state_setFeatState_conc ;
  state_transferMode_abs         = next_state_transferMode_abs ;
  state_stDMACmd_conc            = next_state_stDMACmd_conc ;
  state_regControl_SRST_conc     = next_state_regControl_SRST_conc ;
  state_os_lba0_abs              = next_state_os_lba0_abs ;
  state_os_lba1_abs              = next_state_os_lba1_abs ;
  state_os_lba2_abs              = next_state_os_lba2_abs ;
  state_os_lba3_abs              = next_state_os_lba3_abs ;
  state_os_lba4_abs              = next_state_os_lba4_abs ;
  state_os_lba5_abs              = next_state_os_lba5_abs ;
  state_os_sect0_abs             = next_state_os_sect0_abs ;
  state_os_sect1_abs             = next_state_os_sect1_abs ;
  state_os_buf_abs               = next_state_os_buf_abs ;
  state_osState_conc             = next_state_osState_conc ;
 end
 
 
endmodule