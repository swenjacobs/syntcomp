// IDE hard drive controller specification and operating system interface
// specification for device driver synthesis.
// This file describes a GR(1) game played by a device driver for an IDE hard
// disk against its environment consisting of the hard disk and operating
// system.

`define WIDTH1 2
`define WIDTH2 1

`define CMD_READ_DMA_EXT  1
`define CMD_WRITE_DMA_EXT 2
`define FEAT_WC           1
`define FEAT_NWC          2
`define FEAT_XFR_MODE     3
`define XM_ULTRA_DMA      7

`define RCMD              1
`define REG_FEATURE0      1
`define REG_SECTORS       2
`define REG_LBA_LOW       3
`define REG_LBA_MID       4
`define REG_LBA_HIGH      5
`define REG_DEV           6
`define REG_ERRCMD        7

`define RDMA              0
`define REG_BM_COMMAND    0

//typedef enum {read=0, write=1} i_osReqType_enum;
`define read              0
`define write             1

//typedef enum {write8, write32, fillPrd, reset, os_req, ack_read_succ, ack_read_fail, ack_write_succ, ack_write_fail, class_event} controllable_tag_enum;
`define write8            0
`define write32           1
`define fillPrd           2
`define reset             3
`define os_req            4
`define ack_read_succ     5
`define ack_read_fail     6
`define ack_write_succ    7
`define ack_write_fail    8
`define class_event       9

//typedef enum {wait_bm_ready=0, dma_read=1, bm_read_prd=2, bm_ready=3} state_stDMACmd_enum;
`define wait_bm_ready     0
`define dma_read          1
`define bm_read_prd       2
`define bm_ready          3


//typedef enum {setFeatIdle=0, setFeatWC=1, setFeatNWC=2, setFeatXFR0=3, setFeatXFR1=4} state_setFeatState_enum;
`define setFeatIdle       0
`define setFeatWC         1
`define setFeatNWC        2
`define setFeatXFR0       3
`define setFeatXFR1       4

//typedef enum {os_init=0, os_reset=1, os_write_cache=2, os_idle=3, os_read_pending=4, os_write_pending=5, os_read_ack_succ=6, os_read_ack_fail=7, os_write_ack_succ=8, os_write_ack_fail=9, os_error=10} state_osState_enum;
`define os_init           0
`define os_reset          1
`define os_write_cache    2
`define os_idle           3
`define os_read_pending   4
`define os_write_pending  5
`define os_read_ack_succ  6
`define os_read_ack_fail  7
`define os_write_ack_succ 8
`define os_write_ack_fail 9
`define os_error          10

module ide_hard_drive_controller_2(
        o_err,
        i_clk,
        i_osReqType_conc,
        i_reqLBA0_abs,
        i_reqLBA1_abs,
        i_reqSect0_abs,
        i_reqSect1_abs,
        i_reqBuf_abs,
        i_transSuccess_conc,
        controllable_tag_conc,
        controllable_bank_abs,
        controllable_addr_abs,
        controllable_write8_val_abs,
        controllable_fillPrdAddr_abs,
        controllable_fillPrdNSect_abs,
        controllable_featWCClass_conc,
        controllable_featNWCClass_conc,
        controllable_featXFRClass_conc,
        controllable_busMasterClass_conc,
        controllable_dmaStartClass_conc );

input i_clk;
input i_osReqType_conc ;
input [`WIDTH1:0] i_reqLBA0_abs ;
input [`WIDTH1:0] i_reqLBA1_abs ;
input [`WIDTH1:0] i_reqSect0_abs ;
input [`WIDTH1:0] i_reqSect1_abs ;
input [`WIDTH2:0] i_reqBuf_abs ;
input i_transSuccess_conc ;
input [3:0] controllable_tag_conc ;
input controllable_bank_abs ;
input [2:0] controllable_addr_abs ;
input [`WIDTH1:0] controllable_write8_val_abs ;
input [`WIDTH2:0] controllable_fillPrdAddr_abs ;
input [5:0] controllable_fillPrdNSect_abs ;
input controllable_featWCClass_conc ;
input controllable_featNWCClass_conc ;
input controllable_featXFRClass_conc ;
input controllable_busMasterClass_conc ;
input controllable_dmaStartClass_conc ;
output o_err;

reg [1:0] state_stDMACmd_conc ;
reg [2:0] state_setFeatState_conc ;
reg [1:0] state_regFeature0_abs ;
reg [`WIDTH1:0] state_regSectors0_abs ;
reg [`WIDTH1:0] state_regSectors1_abs ;
reg [`WIDTH1:0] state_regLBALow0_abs ;
reg [`WIDTH1:0] state_regLBALow1_abs ;
reg state_regDev_LBA_abs ;
reg [`WIDTH1:0] state_regCommand_abs ;
reg state_regBMCommand_Start_abs ;
reg state_regBMCommand_RW_abs ;
reg [`WIDTH2:0] state_bufAddr_abs ;
reg [5:0] state_bufSectors_abs ;

reg [3:0] state_osState_conc ;
reg [`WIDTH1:0] state_os_lba0_abs ;
reg [`WIDTH1:0] state_os_lba1_abs ;
reg [`WIDTH1:0] state_os_sect0_abs ;
reg [`WIDTH1:0] state_os_sect1_abs ;
reg [`WIDTH2:0] state_os_buf_abs ;

reg [1:0] fair_cnt;

wire transferMode3;
wire nwc;
wire bm_event;
wire transferCorrect;
wire transferCorrect_0;
wire transferCorrect_1;
wire buechi_satisfied;
wire [1:0] next_state_stDMACmd_conc ;
wire [2:0] next_state_setFeatState_conc ;
wire [1:0] next_state_regFeature0_abs ;
wire [`WIDTH1:0] next_state_regSectors0_abs ;
wire [`WIDTH1:0] next_state_regSectors1_abs ;
wire [`WIDTH1:0] next_state_regLBALow0_abs ;
wire [`WIDTH1:0] next_state_regLBALow1_abs ;
wire next_state_regDev_LBA_abs ;
wire [`WIDTH1:0] next_state_regCommand_abs ;
wire next_state_regBMCommand_Start_abs ;
wire next_state_regBMCommand_RW_abs ;
wire [`WIDTH2:0] next_state_bufAddr_abs ;
wire [5:0] next_state_bufSectors_abs ;

wire [3:0] next_state_osState_conc ;
wire [`WIDTH1:0] next_state_os_lba0_abs ;
wire [`WIDTH1:0] next_state_os_lba1_abs ;
wire [`WIDTH1:0] next_state_os_sect0_abs ;
wire [`WIDTH1:0] next_state_os_sect1_abs ;
wire [`WIDTH2:0] next_state_os_buf_abs ;

// some abbreviations:
assign transferMode3 = (state_setFeatState_conc==`setFeatXFR1 && controllable_featXFRClass_conc==1);
assign nwc =           (state_setFeatState_conc==`setFeatNWC  && controllable_featNWCClass_conc==1);
assign bm_event = (state_stDMACmd_conc==`bm_ready && controllable_busMasterClass_conc==1);
assign transferCorrect = (state_os_lba0_abs==state_regLBALow0_abs    &&
                          state_os_lba1_abs==state_regLBALow1_abs    &&
                          state_os_sect0_abs==state_regSectors0_abs  &&
                          state_os_sect1_abs==state_regSectors1_abs  &&
                          state_os_buf_abs==state_bufAddr_abs);
assign transferCorrect_0 = (transferCorrect      &&
                            state_regBMCommand_RW_abs==0);
assign transferCorrect_1 = (transferCorrect      &&
                            state_regBMCommand_RW_abs==1);


// state updates:
  //Device state updates:
assign next_state_regFeature0_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_FEATURE0) ? controllable_write8_val_abs  : state_regFeature0_abs;
assign next_state_regSectors0_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_SECTORS)  ? controllable_write8_val_abs  : state_regSectors0_abs;
assign next_state_regSectors1_abs  = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_SECTORS)  ? state_regSectors0_abs : state_regSectors1_abs;
assign next_state_regLBALow0_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_LOW)  ? controllable_write8_val_abs  : state_regLBALow0_abs;
assign next_state_regLBALow1_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_LBA_LOW)  ? state_regLBALow0_abs  : state_regLBALow1_abs;
assign next_state_regDev_LBA_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_DEV)      ? controllable_write8_val_abs[2:2] : state_regDev_LBA_abs;
assign next_state_regCommand_abs   = (controllable_tag_conc==`write8 && controllable_bank_abs==`RCMD && controllable_addr_abs==`REG_ERRCMD)   ? controllable_write8_val_abs  : state_regCommand_abs;
assign next_state_regBMCommand_Start_abs = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_COMMAND) ? controllable_write8_val_abs[0:0] : state_regBMCommand_Start_abs;
assign next_state_regBMCommand_RW_abs = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_COMMAND) ? controllable_write8_val_abs[1:1] : state_regBMCommand_RW_abs;
assign next_state_bufAddr_abs = (controllable_tag_conc==`fillPrd) ? controllable_fillPrdAddr_abs : state_bufAddr_abs;
assign next_state_bufSectors_abs = (controllable_tag_conc==`fillPrd) ? controllable_fillPrdNSect_abs : state_bufSectors_abs;

assign next_state_setFeatState_conc = (controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_WC) ? `setFeatWC :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_NWC) ? `setFeatNWC :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_XFR_MODE && state_regSectors0_abs==`XM_ULTRA_DMA) ? `setFeatXFR0 :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatIdle && state_regFeature0_abs==`FEAT_XFR_MODE) ? `setFeatXFR1 :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatWC   && controllable_featWCClass_conc==1) ? `setFeatIdle :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatNWC  && controllable_featNWCClass_conc==1) ? `setFeatIdle :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatXFR0 && controllable_featXFRClass_conc==1) ? `setFeatIdle :
                 ((controllable_tag_conc==`class_event && state_setFeatState_conc==`setFeatXFR1 && controllable_featXFRClass_conc==1) ? `setFeatIdle : state_setFeatState_conc)))))));

assign next_state_stDMACmd_conc = (controllable_tag_conc==`write8 && controllable_bank_abs==`RDMA && controllable_addr_abs==`REG_BM_COMMAND) ?
               (
                 (state_regBMCommand_Start_abs==0 && controllable_write8_val_abs[0:0]==1) ? `bm_read_prd : state_stDMACmd_conc
               ) :
               (
                 (state_stDMACmd_conc==`bm_read_prd) ?
                 (((state_regBMCommand_RW_abs == 1 && state_regCommand_abs != `CMD_READ_DMA_EXT) || (state_regBMCommand_RW_abs == 0 && state_regCommand_abs != `CMD_WRITE_DMA_EXT) || (state_regSectors0_abs != state_bufSectors_abs[`WIDTH1:0]) || (state_regSectors1_abs != state_bufSectors_abs[5:3]) || state_regDev_LBA_abs != 1) ? state_stDMACmd_conc : ((controllable_dmaStartClass_conc==1) ? `bm_ready : state_stDMACmd_conc)) :
                 ((state_stDMACmd_conc==`bm_ready && controllable_busMasterClass_conc==1) ? `wait_bm_ready : state_stDMACmd_conc)
               );

//OS state updates
assign next_state_os_lba0_abs = (controllable_tag_conc==`os_req) ? i_reqLBA0_abs : state_os_lba0_abs;
assign next_state_os_lba1_abs = (controllable_tag_conc==`os_req) ? i_reqLBA1_abs : state_os_lba1_abs;
assign next_state_os_sect0_abs = (controllable_tag_conc==`os_req) ? i_reqSect0_abs : state_os_sect0_abs;
assign next_state_os_sect1_abs = (controllable_tag_conc==`os_req) ? i_reqSect1_abs : state_os_sect1_abs;
assign next_state_os_buf_abs = (controllable_tag_conc==`os_req) ? i_reqBuf_abs : state_os_buf_abs;

assign next_state_osState_conc = (state_osState_conc==`os_init && controllable_tag_conc==`reset) ? `os_reset :
      ((state_osState_conc==`os_reset && nwc && controllable_tag_conc==`class_event) ? `os_write_cache :
      ((state_osState_conc==`os_write_cache && transferMode3 && controllable_tag_conc==`class_event) ? `os_idle :
      ((state_osState_conc==`os_idle && controllable_tag_conc==`os_req && i_osReqType_conc==`read) ? `os_read_pending :
      ((state_osState_conc==`os_idle && controllable_tag_conc==`os_req && i_osReqType_conc==`write) ? `os_write_pending :
      ((state_osState_conc==`os_read_pending && bm_event && !transferCorrect_0 && controllable_tag_conc==`class_event) ? `os_error :
      ((state_osState_conc==`os_read_pending && bm_event && i_transSuccess_conc==1 && controllable_tag_conc==`class_event) ? `os_read_ack_succ :
      ((state_osState_conc==`os_read_pending && bm_event && i_transSuccess_conc==0 && controllable_tag_conc==`class_event) ? `os_read_ack_fail :
      ((state_osState_conc==`os_write_pending && bm_event && !transferCorrect_1 && controllable_tag_conc==`class_event) ? `os_error :
      ((state_osState_conc==`os_write_pending && bm_event && i_transSuccess_conc==1 && controllable_tag_conc==`class_event) ? `os_write_ack_succ :
      ((state_osState_conc==`os_write_pending && bm_event && i_transSuccess_conc==0 && controllable_tag_conc==`class_event) ? `os_write_ack_fail :
      ((state_osState_conc==`os_read_ack_succ && controllable_tag_conc==`ack_read_succ) ? `os_idle :
      ((state_osState_conc==`os_read_ack_fail && controllable_tag_conc==`ack_read_fail) ? `os_idle :
      ((state_osState_conc==`os_write_ack_succ && controllable_tag_conc==`ack_write_succ) ? `os_idle :
      ((state_osState_conc==`os_write_ack_fail && controllable_tag_conc==`ack_write_fail) ? `os_idle : state_osState_conc))))))))))))));


// buechi-to-safety construction:                            
assign buechi_satisfied = (state_osState_conc == `os_idle);                          
                            
assign o_err = (fair_cnt >= 3) || controllable_tag_conc >= 10;

initial
 begin
  state_stDMACmd_conc = `wait_bm_ready;
  state_setFeatState_conc = `setFeatIdle;
  state_regFeature0_abs = 0;
  state_regSectors0_abs = 0;
  state_regSectors1_abs = 0;
  state_regLBALow0_abs = 0;
  state_regLBALow1_abs = 0;
  state_regDev_LBA_abs = 0;
  state_regCommand_abs = 0;
  state_regBMCommand_Start_abs = 0;
  state_regBMCommand_RW_abs = 0;
  state_bufAddr_abs = 0;
  state_bufSectors_abs = 0;
  state_osState_conc = `os_init;
  state_os_lba0_abs = 0;
  state_os_lba1_abs = 0;
  state_os_sect0_abs = 0;
  state_os_sect1_abs = 0;
  state_os_buf_abs = 0;
  fair_cnt = 0;
 end

always @(posedge i_clk)
 begin

  if(buechi_satisfied)
   begin
    fair_cnt = 0;
   end
  else
   begin
    fair_cnt = fair_cnt + 1;
   end
 
  //Device state updates:
  state_regFeature0_abs          = next_state_regFeature0_abs ;
  state_regSectors0_abs          = next_state_regSectors0_abs ;
  state_regSectors1_abs          = next_state_regSectors1_abs ;
  state_regLBALow0_abs           = next_state_regLBALow0_abs ;
  state_regLBALow1_abs           = next_state_regLBALow1_abs ;
  state_regDev_LBA_abs           = next_state_regDev_LBA_abs ;
  state_regCommand_abs           = next_state_regCommand_abs ;
  state_regBMCommand_Start_abs   = next_state_regBMCommand_Start_abs ;
  state_regBMCommand_RW_abs      = next_state_regBMCommand_RW_abs ;
  state_bufAddr_abs              = next_state_bufAddr_abs ;
  state_bufSectors_abs           = next_state_bufSectors_abs ;
  state_setFeatState_conc        = next_state_setFeatState_conc ;
  state_stDMACmd_conc            = next_state_stDMACmd_conc ;
  state_os_lba0_abs              = next_state_os_lba0_abs ;
  state_os_lba1_abs              = next_state_os_lba1_abs ;
  state_os_sect0_abs             = next_state_os_sect0_abs ;
  state_os_sect1_abs             = next_state_os_sect1_abs ;
  state_os_buf_abs               = next_state_os_buf_abs ;
  state_osState_conc             = next_state_osState_conc ;
 end
 
 
endmodule