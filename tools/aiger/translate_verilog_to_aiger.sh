#!/bin/bash

ABC=/home/rkoenighofer/programs/abc/abc_10_04_2013/abc
VL2MV=/home/rkoenighofer/programs/vl2mv/vl2mv-2.4/vl2mv
AIG2AIG=/home/rkoenighofer/programs/aiger/aiger-1.9.4/aigtoaig
IN_DIR=verilog_sources
OUT_DIR=aiger_results

verilog_sources=`find $IN_DIR -name '*.v'`
for verilog_file in ${verilog_sources[@]}
do

  file_name_ext_only=$(basename $verilog_file)
  file_name_only=${file_name_ext_only%.*}

  $VL2MV $verilog_file -o $OUT_DIR/$file_name_only.mv

  # unoptimized:
  $ABC -c "read_blif_mv $OUT_DIR/$file_name_only.mv; write_aiger -s $OUT_DIR/${file_name_only}n.aig"
  $AIG2AIG $OUT_DIR/${file_name_only}n.aig $OUT_DIR/${file_name_only}n.aag
  echo "-------------------------------" >> $OUT_DIR/${file_name_only}n.aag
  echo "This AIGER file has been created by the following sequence of commands:" >> $OUT_DIR/${file_name_only}n.aag
  echo "> vl2mv $file_name_only.v   ---gives--> $file_name_only.mv" >> $OUT_DIR/${file_name_only}n.aag
  echo "> abc -c \"read_blif_mv $file_name_only.mv; write_aiger -s ${file_name_only}n.aig\"   ---gives--> ${file_name_only}n.aig" >> $OUT_DIR/${file_name_only}n.aag
  echo "> aigtoaig ${file_name_only}n.aig ${file_name_only}n.aag   ---gives--> ${file_name_only}n.aag (this file)" >> $OUT_DIR/${file_name_only}n.aag
  echo "Content of $file_name_only.v:" >> $OUT_DIR/${file_name_only}n.aag
  cat $verilog_file >> $OUT_DIR/${file_name_only}n.aag
  echo "" >> $OUT_DIR/${file_name_only}n.aag
  echo "-------------------------------" >> $OUT_DIR/${file_name_only}n.aag

  # optimized
  $ABC -c "read_blif_mv $OUT_DIR/$file_name_only.mv; strash; refactor; rewrite; dfraig; rewrite; dfraig; write_aiger -s $OUT_DIR/${file_name_only}y.aig"
  $AIG2AIG $OUT_DIR/${file_name_only}y.aig $OUT_DIR/${file_name_only}y.aag
  echo "-------------------------------" >> $OUT_DIR/${file_name_only}y.aag
  echo "This AIGER file has been created by the following sequence of commands:" >> $OUT_DIR/${file_name_only}y.aag
  echo "> vl2mv $file_name_only.v   ---gives--> $file_name_only.mv" >> $OUT_DIR/${file_name_only}y.aag
  echo "> abc -c \"read_blif_mv $file_name_only.mv; strash; refactor; rewrite; dfraig; rewrite; dfraig; write_aiger -s ${file_name_only}y.aig\"   ---gives--> ${file_name_only}y.aig" >> $OUT_DIR/${file_name_only}y.aag
  echo "> aigtoaig ${file_name_only}y.aig ${file_name_only}y.aag   ---gives--> ${file_name_only}y.aag (this file)" >> $OUT_DIR/${file_name_only}y.aag
  echo "Content of $file_name_only.v:" >> $OUT_DIR/${file_name_only}y.aag
  cat $verilog_file >> $OUT_DIR/${file_name_only}y.aag
  echo "" >> $OUT_DIR/${file_name_only}y.aag
  echo "-------------------------------" >> $OUT_DIR/${file_name_only}y.aag
done

rm $OUT_DIR/*.mv
rm $OUT_DIR/*.aig